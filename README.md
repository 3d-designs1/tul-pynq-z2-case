This is a case for the PYNQ Z2 Board. It exposes all IO and Ports. It is secured by screw-inserts, but can be adapted. If you need a version without screw inserts let me know.
The Arduino may not fit with this case, but it was not tested.

The design is also available via Printables: https://www.printables.com/model/467479-tul-pynq-z2-case-xilinx-zynq-fpga
You will need screw inserts like these: https://amzn.eu/d/dT83Ca7 (6mm long M3) and screws like these https://amzn.eu/d/beGwsAC (12mm long with 6mm diameter Head).

Happy printing!
